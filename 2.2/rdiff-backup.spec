# -*- mode: python ; coding: utf-8 -*-


block_cipher = None

import shutil

from debbuild import debbuild
from PyInstaller.utils.hooks import collect_submodules

location = shutil.which("rdiff-backup")

a = Analysis(
    [location],
    pathex=[],
    binaries=[],
    datas=[],
    hiddenimports=collect_submodules("rdiffbackup")
    + ["xattr", "posix1e", "psutil"],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name="rdiff-backup",
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name="rdiff-backup",
)

from email import message_from_string

# Read package info
import pkg_resources

pkg = pkg_resources.get_distribution("rdiff-backup")
version = pkg.version
_metadata = message_from_string(pkg.get_metadata("METADATA"))
pkg_info = dict(_metadata.items())
long_description = _metadata._payload

# Also create a Debian package
debbuild(
    name="rdiff-backup-2.2",
    version=version + "-5",
    architecture="amd64",
    data_src=[
        ("/opt/rdiff-backup-2.2", "./dist/rdiff-backup"),
    ],
    description=pkg_info["Summary"],
    long_description=long_description,
    url=pkg_info["Home-page"],
    maintainer="%s <%s>" % (pkg_info["Author"], pkg_info["Author-email"]),
    output="./dist",
    symlink=[
        ("/usr/bin/rdiff-backup-2.2", "/opt/rdiff-backup-2.2/rdiff-backup"),
    ],
    depends=["libc6"],
)
