# Rdiff-backup Build

This project is used to generate binaries and packages for
rdiff-backup v1.2.8 to be used by [Rdiffweb](https://rdiffweb.org) and
[Minarca](https://minarca.org).

## Download / Install

Download from APT repository

    apt update
    apt upgrade
    apt install apt-transport-https ca-certificates curl lsb-release gpg
    curl -L https://www.ikus-soft.com/archive/minarca/public.key | gpg --dearmor > /usr/share/keyrings/minarca-keyring.gpg
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/minarca-keyring.gpg] https://nexus.ikus-soft.com/repository/apt-release-$(lsb_release -sc)/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/minarca.list
    apt update
    apt install rdiff-backup-1.2 rdiff-backup-2.0 rdiff-backup-2.2

## Changelog

## 2024-11-12

* Add support for Ubuntu Noble 24.04 LTS & Oracular 24.10

## 2024-10-17

* Add patch to handle BlockingIOError & TimeoutError for 2.2
* Add patch to handle BlockingIOError & TimeoutError for 2.0
* Add patch to continue on cross device link exception for 2.0

## 2024-10-10

* Add patch to continue on cross device link exception for 2.2

## 2023-11-27

* Add patch to improve regression speed for 2.2
* Include optional dependencies `psutil`

## 2023-10-27

* Add build for Debian Trixie

## 2023-09-08

* Upgrade rdiff-backup v2.2 to 2.2.6

### 2023-08-04

* Add support for Ubuntu Mantic

### 2023-05-26

* Upgrade rdiff-backup v2.2 to 2.2.5

### 2023-04-28

* Add support for rdiff-backup v2.2.x
* Add support for Ubuntu Lunar

### 2022-11-28

* Add support for Ubuntu Kinetic

### 2021-02-19

* Generate build for ubuntu:groovy
* Add patch for cross-device links [#519](https://github.com/rdiff-backup/rdiff-backup/issues/519)

### 2020-09-01 - Debian packages

* Generate v1.2.8 Debian packages for x86_64 and publish to Nexus server

### 2019-06-14 - Initial version

* Generate v1.2.8 binaries for Windows

## TODO

* Cross compile for other architecture: x86, arm
